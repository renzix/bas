package bas

import java.io._
import java.nio.file.{Path,Files}

import scalafx.Includes._
import scalafx.application.JFXApp
import scalafx.event.ActionEvent
import scalafx.scene.Scene
import scalafx.scene.control._
import scalafx.scene.layout.BorderPane
import scalafx.stage.FileChooser

object ScalaFXHelloWorld extends JFXApp {
  // Text Area
  val main = new TextArea
  var openFile : Option[Path] = None
  // Menus
  val fileMenu = new Menu("File") {
    items = List(
      new MenuItem("Open") {
        onAction = (ae: ActionEvent) => {
          val f = new FileChooser {
            title = "Re Open File"
          }
          val loc = f.showOpenDialog(stage).toPath()
          main.setText(new String(Files.readAllBytes(loc),"UTF-8"))
          openFile = Some(loc)
        }
      },
      new MenuItem("Save") {
        onAction = (ae: ActionEvent) => {
          val f = new FileChooser {
            title = "Re Save File"
          }
          val pw =
            new PrintWriter (
              new File (
                if (openFile.isDefined) {
                  openFile.get.toString
                } else {
                  new FileChooser { title = "Re Open File" }
                    .showOpenDialog(stage).toString
                }))
          pw.write(main.getText)
          pw.close
        }
      },
      new MenuItem("Close") {
        onAction = (ae: ActionEvent) => System.exit(0)
      }
    )
  }
  stage =
    new JFXApp.PrimaryStage {
      title.value = "Basic Text Editor"
      width = 600
      height = 450
      // menubar = new JMenuBar {}
      scene = new Scene {
        root = new BorderPane {
          top = new MenuBar {
            useSystemMenuBar = true
            minWidth = 100
            menus.add(fileMenu)
          }
          center = main
        }
      }
    }
}
